var express=require('express'); /*importamos librerias*/
var app=express();              /*iniciando el framework web*/
var port=process.env.PORT || 3000;
var bodyParser=require('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto "+port);

app.get("/apitechu/v1",
  function(req,res){
      console.log("GET /apitechu/v1");
      res.send('{"msg":"Hola prueba"}');
    }
  );

app.get("/apitechu/v1/users",
    function(req,res){
        console.log("GET /apitechu/v1/users");
        res.sendFile('usuarios.json',{root:__dirname});
        //var users=require('./usuarios.json');
        //res.send(users);
      }
    );

app.post("/apitechu/v1/users",
  function(req,res){
    console.log("POST apitechu/v1/users");
    //console.log(req.headers);
    console.log("First name es "+req.body.first_name);
    console.log("Last name es "+req.body.last_name);
    console.log("Country es "+req.body.country);

    var newUser={
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "country":req.body.country
    };

    var users=require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);
    console.log("Usuario guardado con éxito");
    res.send({"msg":"Usuario guardado con éxito"});
  }

);

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("Parámetros");
    console.log(req.params);
    console.log("Query String");
    console.log(req.query);
    console.log("Body");
    console.log(req.body);
    console.log("Headers");
    console.log(req.headers);
  }
);

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE apitechu/v1/users/:id");
    //console.log(req.params);
    console.log(req.params.id);

    var users=require('./usuarios.json');
    users.splice(req.params.id-1,1);
    writeUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg":"Usuario borrado"});


  }


);

app.post("/apitechu/v1/login",
  function(req,res){
    console.log("POST /apitechu/v1/login");
    var email=req.body.email;
    var pass=req.body.password;
    console.log("email es " + email);
    console.log("password es " + pass);

    var users=require('./usuarios.json');
    var msg="Login incorrecto";
    for(user of users){
      if(user.email==email&&user.password==pass){
        user.logged=true;
        var id=user.id;
        console.log("Login correcto "+user.id);
        msg="Login correcto";
        writeUserDataToFile(users);
      }
    }
    res.send({"mensaje":msg ,"idUsuario":id});

  }
);



app.post("/apitechu/v1/logout",
  function(req,res){
    console.log("POST /apitechu/v1/logout");
    var id=req.body.id;
    console.log("id"+id);
    var users=require('./usuarios.json');
    var msg="Login incorrecto";
    var flag=0;
    for(user of users){
      if(user.id==id&&user.logged==true){
        msg="Login correcto";
        flag=1;
        delete user.logged;
        writeUserDataToFile(users);
      }

    }
    if(flag==0) id=undefined;
    res.send({"mensaje":msg ,"idUsuario":id});
  }

);

function writeUserDataToFile(data){
  var fs=require('fs');
  var jsonUserData=JSON.stringify(data);
  fs.writeFile("./usuarios.json",jsonUserData,"utf-8",function(err){
    if(err){
      console.log(err);
    }else{
      console.log("Datos escritos en archivo");
    }
  }
);
}
